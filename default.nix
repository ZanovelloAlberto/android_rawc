let
  unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) { };
in
{ pkgs ? import <nixpkgs> { overlays = (import ./nix/overlays); } }:
let


  abiVersions = [ "armeabi-v7a" "arm64-v8a" "x86" "x86_64" ];
  platformVersions = [ "26" ];
  platformToolsVersion = "34.0.5";
  toolsVersion = "26.1.1";
  buildToolsVersions = [ "30.0.3" ];
  myand = pkgs.androidenv.composeAndroidPackages rec {

    inherit
      abiVersions
      toolsVersion
      buildToolsVersions
      platformToolsVersion
      platformVersions;


    includeNDK = true;
    useGoogleAPIs = false;
    useGoogleTVAddOns = false;
    includeSources = false;
    includeSystemImages = false;
    includeEmulator = false;
    # emulatorVersion = "30.3.4";
    # ndkVersions = [ "22.0.7026061" ];
  };

in


pkgs.stdenv.mkDerivation rec {
  ANDROIDSDK = "${myand.androidsdk}/libexec/android-sdk";
  ADB = "${myand.androidsdk}/bin/adb";
  NDK = "${ANDROIDSDK}/ndk-bundle";

  ANDROIDVERSION = (builtins.head platformVersions);

  pname = "sample";
  version = "0.1.1";
  buildInputs = with pkgs;[ gnumake jdk gettext zip myand.platform-tools ];
  src = ./.;
  RAWDRAW = fetchGit {
    url = "https://github.com/cntools/rawdraw";
    rev = "e297d7e18fbfb6034bc14ac8232d346d9e1efa63";
  };
  # CNFA = fetchGit {
  #   url = "https://github.com/cntools/cnfa.git";
  #   rev = "1eaa85bda69f01b29e8652c46c586ca46392387c";
  # }
  buildPhase = ''
    make makecapk.apk
  '';
  shellHook = with pkgs;''
    ${lib.add_lib "${NDK}/toolchains/llvm/prebuilt/linux-x86_64/lib/"}
    echo "${
    lib.wrap_with_lib [libcxx] "echo $LD_LIBRARY_PATH"
    }"
  '';
}
