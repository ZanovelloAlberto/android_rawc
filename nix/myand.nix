{ pkgs }:

let
  abiVersions = [ "armeabi-v7a" "arm64-v8a" "x86" "x86_64" ];
  platformVersions = [ "26" ];
  platformToolsVersion = "34.0.5";
  toolsVersion = "26.1.1";
  buildToolsVersions = [ "30.0.3" ];
  myand = pkgs.androidenv.composeAndroidPackages rec {

    inherit
      abiVersions
      toolsVersion
      buildToolsVersions
      platformToolsVersion
      platformVersions;

    includeNDK = true;
    useGoogleAPIs = false;
    useGoogleTVAddOns = false;
    includeSources = false;
    includeSystemImages = false;
    includeEmulator = false;
    # emulatorVersion = "30.3.4";
    # ndkVersions = [ "22.0.7026061" ];
  };

in

{
  inherit abiVersions
    platformVersions
    platformToolsVersion
    toolsVersion
    buildToolsVersions
    myand;
    }
