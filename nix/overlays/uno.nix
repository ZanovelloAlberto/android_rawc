final: prev:
{
  zig_0_6 = prev.zig.overrideAttrs (old: {
    # CMAKE_GENERATOR = "Ninja";
    # version = "0.6.0";
    # nativeBuildInputs = (old.nativeBuildInputs or []) ++ [ prev.ninja];
    patches = (old.patches or [ ]) ++ [
      # (prev.fetchpatch {
      #   url = "https://github.com/charlieLehman/sl/commit/e20abbd7e1ee26af53f34451a8f7ad79b27a4c0a.patch";
      #   hash = "07sx98d422589gxr8wflfpkdd0k44kbagxl3b51i56ky2wfix7rc";
      # })
      # alternatively if you have a local patch,
      # /path/to/file.patch
      # or a relative path (relative to the current nix file)
      ./hpp.patch
    ];
  });
}